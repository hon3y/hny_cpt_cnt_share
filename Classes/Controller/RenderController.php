<?php
declare(strict_types=1);
namespace HIVE\HiveCptCntShare\Controller;

/***
 *
 * This file is part of the "hive_cpt_cnt_share" Extension for TYPO3 CMS.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 *
 *  (c) 2017
 *
 ***/

/**
 * RenderController
 */
class RenderController extends \TYPO3\CMS\Extbase\Mvc\Controller\ActionController
{
    /**
     * renderRepository
     *
     * @var \HIVE\HiveCptCntShare\Domain\Repository\RenderRepository
     * @inject
     */
    protected $renderRepository = null;

    /**
     * action render
     *
     * @return void
     */
    public function renderAction()
    {
        /** @var array $aError */
        $aError = [0,''];

        /** @var array $aSettings */
        $aSettings = $this->settings;

        /** @var array $aHtml */
        $aHtml = [];

        /** @var string $sUrl */
        $sUrl =  urlencode((isset($_SERVER['HTTPS']) ? "https" : "http") . "://{$_SERVER['HTTP_HOST']}{$_SERVER['REQUEST_URI']}");

        /** @var boolean $bShowShareIcons */
        $bShowShareIcons = true;

        /** @var array $aProviders */
        $aProviders = [
            [
                'facebook',
                "https://facebook.com/sharer.php?u={$sUrl}",
                "https://facebook.com/sharer.php?u={$sUrl}", //noscript
                "_blank",
            ],
            [
                'twitter',
                "https://twitter.com/intent/tweet?url={$sUrl}&text=###TITLE###+###DESCRIPTION###",
                "https://twitter.com/intent/tweet?url={$sUrl}", //noscript
                "_blank",
            ],
            [
                'linkedin',
                "http://www.linkedin.com/shareArticle?mini=true&url={$sUrl}&title=###TITLE###&summary=###DESCRIPTION###",
                "http://www.linkedin.com/shareArticle?mini=true&url={$sUrl}", //noscript
                "_blank",
            ],
            [
                'xing',
                "https://www.xing-share.com/app/user?op=share;sc_p=xing-share;url={$sUrl}",
                "https://www.xing-share.com/app/user?op=share;sc_p=xing-share;url={$sUrl}", //noscript
                "_blank",
            ],
            [
                'mailto',
                "mailto:?subject=###TITLE###&body=###DESCRIPTION###%0D%0A%0D%0A{$sUrl}",
                "mailto:?", //noscript
                "_self",
            ]
        ];

        if (array_key_exists('share', $aSettings) && is_array($aSettings['share'])) {

            if(array_key_exists('excludeIconsForDomainParts', $aSettings['share']) && $aSettings['share']['excludeIconsForDomainParts'] != "") {
                $aExcludeIconsForDomainParts = explode(",", $aSettings['share']['excludeIconsForDomainParts']);
                foreach ($aExcludeIconsForDomainParts as $sExcludeIconsForDomainPart) {
                    if (strpos($sUrl, urlencode($sExcludeIconsForDomainPart)) !== FALSE) {
                        $bShowShareIcons = false;
                    }
                }
            }

            if ($bShowShareIcons) {
                foreach ($aProviders as $k => $aProvider) {
                    if (array_key_exists($aProvider[0], $aSettings['share']) && is_array($aSettings['share'][$aProvider[0]])) {
                        $aHtml[$aProvider[0]] = [];
                        if (array_key_exists('active', $aSettings['share'][$aProvider[0]])) {
                            $aHtml[$aProvider[0]]['active'] = $aSettings['share'][$aProvider[0]]['active'];
                        } else {
                            $aError = [1, __CLASS__ . ' ' . __METHOD__ . ' ' . __LINE__];
                        }
                        if (array_key_exists('icon', $aSettings['share'][$aProvider[0]])) {
                            $aHtml[$aProvider[0]]['icon'] = $aSettings['share'][$aProvider[0]]['icon'];
                        } else {
                            $aError = [1, __CLASS__ . ' ' . __METHOD__ . ' ' . __LINE__];
                        }
                        $aHtml[$aProvider[0]]['href'] = $aProvider[1];
                        $aHtml[$aProvider[0]]['hrefNoscript'] = $aProvider[2];
                        $aHtml[$aProvider[0]]['target'] = $aProvider[3];
                    } else {
                        $aError = [1, __CLASS__ . ' ' . __METHOD__ . ' ' . __LINE__];
                    }
                }
            }

        } else {
            $aError = [1, __CLASS__ . ' ' . __METHOD__ . ' ' . __LINE__];
        }

        $this->view->assign('aHtml', $aHtml);
        $this->view->assign('bShowShareIcons', ($bShowShareIcons ? 1 : 0));
        $this->view->assign('aError', $aError);
    }

}
