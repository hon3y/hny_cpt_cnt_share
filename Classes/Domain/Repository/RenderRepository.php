<?php
namespace HIVE\HiveCptCntShare\Domain\Repository;

/***
 *
 * This file is part of the "hive_cpt_cnt_share" Extension for TYPO3 CMS.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 *
 *  (c) 2017
 *
 ***/

/**
 * The repository for Renders
 */
class RenderRepository extends \TYPO3\CMS\Extbase\Persistence\Repository
{
    }
