<?php
defined('TYPO3_MODE') || die('Access denied.');

call_user_func(
    function()
    {

        \TYPO3\CMS\Extbase\Utility\ExtensionUtility::configurePlugin(
            'HIVE.HiveCptCntShare',
            'Hivecptcntsharerender',
            [
                'Render' => 'render'
            ],
            // non-cacheable actions
            [
                'Render' => 'render'
            ]
        );

    // wizards
    \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addPageTSConfig(
        'mod {
            wizards.newContentElement.wizardItems.plugins {
                elements {
                    hivecptcntsharerender {
                        icon = ' . \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::extRelPath('hive_cpt_cnt_share') . 'Resources/Public/Icons/user_plugin_hivecptcntsharerender.svg
                        title = LLL:EXT:hive_cpt_cnt_share/Resources/Private/Language/locallang_db.xlf:tx_hive_cpt_cnt_share_domain_model_hivecptcntsharerender
                        description = LLL:EXT:hive_cpt_cnt_share/Resources/Private/Language/locallang_db.xlf:tx_hive_cpt_cnt_share_domain_model_hivecptcntsharerender.description
                        tt_content_defValues {
                            CType = list
                            list_type = hivecptcntshare_hivecptcntsharerender
                        }
                    }
                }
                show = *
            }
       }'
    );
    }
);
## EXTENSION BUILDER DEFAULTS END TOKEN - Everything BEFORE this line is overwritten with the defaults of the extension builder