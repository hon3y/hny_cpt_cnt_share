<?php
defined('TYPO3_MODE') || die('Access denied.');

call_user_func(
    function()
    {

        \TYPO3\CMS\Extbase\Utility\ExtensionUtility::registerPlugin(
            'HIVE.HiveCptCntShare',
            'Hivecptcntsharerender',
            'hive_cpt_cnt_share :: Render'
        );

        \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addStaticFile('hive_cpt_cnt_share', 'Configuration/TypoScript', 'hive_cpt_cnt_share');

        \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addLLrefForTCAdescr('tx_hivecptcntshare_domain_model_render', 'EXT:hive_cpt_cnt_share/Resources/Private/Language/locallang_csh_tx_hivecptcntshare_domain_model_render.xlf');
        \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::allowTableOnStandardPages('tx_hivecptcntshare_domain_model_render');

    }
);
## EXTENSION BUILDER DEFAULTS END TOKEN - Everything BEFORE this line is overwritten with the defaults of the extension builder